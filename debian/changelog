libhtml-widget-perl (1.11-7) unstable; urgency=medium

  * Run tests during build and autopkgtest with PERL_USE_UNSAFE_INC=0
    as a workaround for perl bug #1043344.
    Thanks to Niko Tyni for the analysis.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Thu, 10 Aug 2023 14:01:37 +0200

libhtml-widget-perl (1.11-6) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Add missing build dependency on libmodule-install-perl.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on libhtml-tree-perl.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 14 Jun 2022 22:56:57 +0100

libhtml-widget-perl (1.11-5) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Change bugtracker URL(s) to HTTPS.
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Niko Tyni ]
  * Update to debhelper compat level 10
  * Update to Standards-Version 4.1.3
  * Declare that the package does not need (fake)root to build
  * Add Testsuite declaration for autopkgtest-pkg-perl

 -- Niko Tyni <ntyni@debian.org>  Sun, 25 Feb 2018 10:53:06 +0200

libhtml-widget-perl (1.11-4) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * debian/watch: remove obsolete comment.

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Add patch to fix hash randomisation issue with perl 5.18 in tests.
    Add build dependency on libtest-deep-perl.
    (Closes: #711572)
  * Update years of packaging copyright.
  * Bump debhelper compatibility level to 8.
  * Drop build dependency on libmodule-install-perl.
  * Stop explicitly enabling POD test. Meant for authors at release time.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Tue, 29 Oct 2013 20:34:52 +0100

libhtml-widget-perl (1.11-3) unstable; urgency=low

  [ Ansgar Burchardt ]
  * Update my email address.

  [ gregor herrmann ]
  * New patch to make tests compatible with libhtml-tree-perl >= 4; make build
    dependency versioned (closes: #605809).
  * Set Standards-Version to 3.9.1; remove perl version from build deps.
  * Add /me to Uploaders.
  * Switch to source format 3.0 (quilt).
  * Refresh debian/rules.
  * debian/copyright: update formatting.
  * debian/control: move libmodule-pluggable-fast-perl from Depends to
    Suggests (only used in an example).
  * Add a patch to fix a spelling mistake.

 -- gregor herrmann <gregoa@debian.org>  Wed, 19 Jan 2011 18:49:45 +0100

libhtml-widget-perl (1.11-2) unstable; urgency=low

  [ Ryan Niebur ]
  * Take over for the Debian Perl Group
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza);
    ${misc:Depends} to Depends: field. Changed: Maintainer set to Debian
    Perl Group <pkg-perl-maintainers@lists.alioth.debian.org> (was:
    Debian Catalyst Maintainers <pkg-catalyst-
    maintainers@lists.alioth.debian.org>); Debian Catalyst Maintainers
    <pkg-catalyst-maintainers@lists.alioth.debian.org> moved to
    Uploaders.
  * debian/watch: use dist-based URL.
  * Remove Florian Ragwitz from Uploaders (Closes: #523383)

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ansgar Burchardt ]
  * Refresh rules for debhelper 7.
  * Do no longer install README (no useful content).
  * Add myself to Uploaders.
  * Bump Standards-Version to 3.8.2.
  * Convert debian/copyright to proposed machine-readable format.
  * Disable POD tests for now: coverage tests fail.
  * Add patch to fix whatis entry for HTML::Widget::Manual::Developer
    + Add quilt framework, add README.source

  [ gregor herrmann ]
  * debian/control: slightly change short and long description.
  * New patch pod-coverage-test.patch: use a different environment variable
    for pod and pod coverage tests, so we can enable them separately.

 -- Ansgar Burchardt <ansgar@43-1.org>  Mon, 20 Jul 2009 15:09:59 +0200

libhtml-widget-perl (1.11-1) unstable; urgency=low

  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Fri,  9 Mar 2007 23:04:01 +0100

libhtml-widget-perl (1.10-1) unstable; urgency=low

  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Mon,  2 Oct 2006 12:16:05 +0200

libhtml-widget-perl (1.09-1) unstable; urgency=low

  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Fri, 22 Sep 2006 11:33:45 +0200

libhtml-widget-perl (1.08-1) unstable; urgency=low

  * New upstream release
  * debian/control:
   + Standards-Version: updated to 3.7.2.1
  * debian/compat:
   + Updated to 5

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Wed, 16 Aug 2006 19:10:55 +0200

libhtml-widget-perl (1.07-2) unstable; urgency=low

  * debian/control: Updated dependencies, libclass-data-accessor-perl added
    (closes: #365169)

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Fri, 28 Apr 2006 15:23:18 +0200

libhtml-widget-perl (1.07-1) unstable; urgency=low

  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Thu, 27 Apr 2006 12:44:06 +0200

libhtml-widget-perl (1.06-1) unstable; urgency=low

  * New upstream release
  * debian/control: libmodule-pluggable-fast-perl and
    libtest-nowarnings-perl added to dependencies

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Tue, 11 Apr 2006 20:09:27 +0200

libhtml-widget-perl (1.05-1) unstable; urgency=low

  * New upstream release

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Mon, 13 Mar 2006 16:41:53 +0100

libhtml-widget-perl (1.04-1) unstable; urgency=low

  * New upstream release
  * debian/rules - some cleaning with make test

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Wed,  8 Feb 2006 23:28:51 +0100

libhtml-widget-perl (1.03-1) unstable; urgency=low

  * New upstream release (closes: #351640)

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Fri,  3 Feb 2006 20:10:15 +0100

libhtml-widget-perl (1.01-1) unstable; urgency=low

  * Initial release.

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Wed, 11 Jan 2006 16:47:53 +0100
